package com.example.project_timeplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project_timeplanner.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.zip.DataFormatException;

public class AddNewTask extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    EditText addNewTaskSubject;
    EditText addNewTaskDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);

        databaseHelper = new DatabaseHelper(this);

        addNewTaskSubject = findViewById(R.id.editTextAddTaskSubject);
        addNewTaskDescription = findViewById(R.id.editTextDescription);

        Button btn = findViewById(R.id.submitTaskBtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addNewTaskSubject.getText().toString().isEmpty()){
                    Toast.makeText(getBaseContext(), "Add task subject",
                            Toast.LENGTH_LONG).show();
                }
//                else if(addNewTaskDescription.getText().toString().isEmpty()){
//                    Toast.makeText(getBaseContext(), "Add task description",
//                            Toast.LENGTH_LONG).show();
//                }
                else{
                    databaseHelper.addTask(addNewTaskSubject.getText().toString(),addNewTaskDescription.getText().toString());
                }
            }
        });
    }
}
