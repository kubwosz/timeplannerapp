package com.example.project_timeplanner.task;

public class Task {
     Integer Id;
     String Subject;
     String Description;

    public Task(Integer id, String sub, String des){
        Id = id;
        Subject = sub;
        Description = des;
    }

    public String getSubject() {
        return Subject;
    }
    public String getDescription() {
        return Description;
    }
    public Integer getId() {
        return Id;
    }
}
