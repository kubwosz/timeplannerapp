package com.example.project_timeplanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.project_timeplanner.task.Task;

import java.util.ArrayList;

import javax.security.auth.Subject;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "timePlannerDB";
    private static final String TABLE_NAME = "tasks";

   public DatabaseHelper(Context context){
        super(context,DATABASE_NAME,null,1);
   }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "create table " + TABLE_NAME + "(id INTEGER PRIMARY KEY AUTOINCREMENT, subject TEXT, description TEXT)";

        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
    onCreate(db);
    }

    public void deleteAllData(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        String deleteAllData = "delete from " + TABLE_NAME;
        sqLiteDatabase.execSQL(deleteAllData);
    }

    public void deleteTask(int id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        String deleteTask = "delete from " + TABLE_NAME + " where id = " + id;
        sqLiteDatabase.execSQL(deleteTask);
    }

    public boolean addTask(String subject, String description){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("subject",subject);
        contentValues.put("description",description);

        sqLiteDatabase.insert(TABLE_NAME,null,contentValues);

        return true;
    }

    public ArrayList getAllTasks(){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ArrayList<Task> arrayList = new ArrayList<>();

        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+TABLE_NAME,null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
        arrayList.add(new Task(cursor.getInt(cursor.getColumnIndex("id")),cursor.getString(cursor.getColumnIndex("subject")), cursor.getString(cursor.getColumnIndex("description"))));
        cursor.moveToNext();
         }
        return arrayList;
    }

    public ArrayList getAllIds(){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        ArrayList<Integer> arrayList = new ArrayList<>();

        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+TABLE_NAME,null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            arrayList.add(Integer.parseInt(cursor.getString(cursor.getColumnIndex("id"))));
            cursor.moveToNext();
        }
        return arrayList;
    }
}
