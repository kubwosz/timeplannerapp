package com.example.project_timeplanner.ui.home;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.project_timeplanner.AddNewTask;
import com.example.project_timeplanner.DatabaseHelper;
import com.example.project_timeplanner.EditTask;
import com.example.project_timeplanner.MainActivity;
import com.example.project_timeplanner.R;
import com.example.project_timeplanner.task.Task;

import java.util.ArrayList;


public class HomeFragment extends Fragment {
    private HomeViewModel homeViewModel;
    private ListView listView;
    //String  titles[] = {"dsad","Dsadas","Dasdas"};
    ArrayList<Task> listOfTasks;
    ArrayList<Integer> listOfTaskIds;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        listOfTasks = databaseHelper.getAllTasks();
        listOfTaskIds = databaseHelper.getAllIds();

        //databaseHelper.deleteAllData();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        listOfTasks = databaseHelper.getAllTasks();
        listOfTaskIds = databaseHelper.getAllIds();

        listView = getActivity().findViewById(R.id.list1);

        MyAdapter adapter = new MyAdapter(getActivity(), listOfTasks);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity().getApplicationContext(), EditTask.class);
                intent.putExtra("id", listOfTaskIds.get(position));
                startActivity(intent);
            }
        }));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        listView = getActivity().findViewById(R.id.list1);

        MyAdapter adapter = new MyAdapter(getActivity(), listOfTasks);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getActivity().getApplicationContext(), EditTask.class);
                    intent.putExtra("id", listOfTaskIds.get(position));
                    startActivity(intent);
            }
        }));
    }

    private class MyAdapter extends ArrayAdapter<String> {
        Context context;
        ArrayList<Task> arrListOfTasks;
        Integer pstn;

        MyAdapter(Context c, ArrayList listOfTasks){
            super(c,R.layout.row, R.id.textViewSubject, listOfTasks);
            this.context = c;
            this.arrListOfTasks = listOfTasks;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row,parent,false);

            TextView subjectTV, descriptionTV;

            subjectTV = row.findViewById(R.id.textViewSubject);
            descriptionTV = row.findViewById(R.id.textViewDescription);

            subjectTV.setText(arrListOfTasks.get(position).getSubject());
            descriptionTV.setText(arrListOfTasks.get(position).getDescription());

            pstn = position;
            ImageView imgDel = row.findViewById(R.id.deleteTaskImg);
            imgDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
                    databaseHelper.deleteTask(arrListOfTasks.get(pstn).getId());
                }
            });

            if(descriptionTV.getText().toString().isEmpty()){
                descriptionTV.setVisibility(View.GONE);
            }
            return row;
        }
    }

}

